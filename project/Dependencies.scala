import sbt._

object Versions {

  final val catsEffect             = "2.1.1"
  final val fs2Rabbit              = "2.1.1"
  final val http4s                 = "0.21.1"
  final val logback                = "1.2.3"
  final val logstashLogbackEncoder = "6.1"
  final val scalaOpentracing       = "0.1.1"
  final val scalactic              = "3.0.8"
  final val scalaLogging           = "3.9.2"
  final val tapir                  = "0.12.23"

}

object Dependencies {

  final val catsEffect = "org.typelevel" %% "cats-effect" % Versions.catsEffect

  final val fs2Rabbit = "dev.profunktor" %% "fs2-rabbit" % Versions.fs2Rabbit

  object Http4s {

    final val blazeClient = "org.http4s" %% "http4s-blaze-client" % Versions.http4s
    final val core        = "org.http4s" %% "http4s-core"         % Versions.http4s

    final val all = List(blazeClient, core)

  }

  object Log {

    final val logback                = "ch.qos.logback"             % "logback-classic"          % Versions.logback
    final val scalaLogging           = "com.typesafe.scala-logging" %% "scala-logging"           % Versions.scalaLogging
    final val logstashLogbackEncoder = "net.logstash.logback"       % "logstash-logback-encoder" % Versions.logstashLogbackEncoder

    final val all = List(logback, logstashLogbackEncoder, scalaLogging)

  }

  object ScalaOpentracing {

    final val core  = "com.colisweb" %% "scala-opentracing"                   % Versions.scalaOpentracing
    final val tapir = "com.colisweb" %% "scala-opentracing-tapir-integration" % Versions.scalaOpentracing

    final val all = List(core, tapir)

  }

  final val scalactic = "org.scalactic" %% "scalactic" % Versions.scalactic

  final val tapirCore = "com.softwaremill.sttp.tapir" %% "tapir-core" % Versions.tapir

}
