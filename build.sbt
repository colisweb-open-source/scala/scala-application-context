import Dependencies._
import TestDependencies._

name := "scala-application-context"

ThisBuild / organization := "com.colisweb"

ThisBuild / scalaVersion := "2.12.10"

ThisBuild / releaseCommitMessage := s"[sbt-release] Set version to ${(version in ThisBuild).value}"

ThisBuild / scalafmtOnCompile := true
ThisBuild / scalafmtCheck := true
ThisBuild / scalafmtSbtCheck := true

ThisBuild / resolvers += Resolver.bintrayRepo("colisweb", "maven")

ThisBuild / bintrayOrganization := Some("colisweb")
ThisBuild / licenses += ("Apache-2.0", url("http://www.apache.org/licenses/"))

ThisBuild / fork in Test := true

lazy val scalaApplicationContext = Project(id = "scala-application-context", base = file("."))
  .aggregate(amqp, core, httpServer, httpClient, infrastructure, httpTest)
  .settings(
    skip in publish := true
  )

lazy val core = Project(id = "scala-application-context-core", base = file("core"))
  .settings(
    libraryDependencies ++= List(catsEffect) ++ Log.all
  )

lazy val infrastructure = Project(id = "scala-application-context-infrastructure", base = file("infrastructure"))
  .dependsOn(core)
  .settings(
    libraryDependencies ++= List(ScalaOpentracing.core)
  )

lazy val httpServer = Project(id = "scala-application-context-http-server", base = file("http/server"))
  .dependsOn(core, infrastructure)
  .settings(
    libraryDependencies ++= List(Http4s.core, scalatest, tapirCore) ++ ScalaOpentracing.all
  )

lazy val httpClient = Project(id = "scala-application-context-http-client", base = file("http/client"))
  .dependsOn(core, infrastructure)
  .settings(
    libraryDependencies ++= List(Http4s.blazeClient)
  )

lazy val httpTest = Project(id = "scala-application-context-http-test", base = file("http/test"))
  .dependsOn(httpClient, httpServer)
  .settings(
    libraryDependencies ++= List(http4sCirce, requests, scalatest, tapirJsonCirce, wiremock) ++ Circe.all,
    scalacOptions ++= Seq("-Ypartial-unification"),
    skip in publish := true
  )

lazy val amqp = Project(id = "scala-application-context-amqp", base = file("amqp"))
  .dependsOn(core, infrastructure)
  .settings(
    libraryDependencies ++= List(fs2Rabbit, scalatest)
  )
